﻿using System;
using Microsoft.WindowsAzure.Storage.Table;

namespace DemoAzureFunction.Entities
{
    public class CustomersTableEntity : TableEntity
    {
        public DateTime CreatedTime { get; set; }
        public string Name { get; set; }
    }
}
