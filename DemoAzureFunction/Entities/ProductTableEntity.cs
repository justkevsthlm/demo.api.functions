﻿using System;
using Microsoft.WindowsAzure.Storage.Table;

namespace DemoAzureFunction.Entities
{
    public class ProductTableEntity : TableEntity
    {
        public DateTime CreatedTime { get; set; }
        public string Description { get; set; }
        public string Price { get; set; }
        public string Name { get; set; }
        public bool OutDated { get; set; }
    }
}
