//using System.IO;
//using System.Threading.Tasks;
//using DemoAzureFunctionVisma.Application.Product;
//using DemoAzureFunctionVisma.Entities;
//using DemoAzureFunctionVisma.Models;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.Azure.WebJobs;
//using Microsoft.Azure.WebJobs.Extensions.Http;
//using Microsoft.Extensions.Logging;
//using Microsoft.WindowsAzure.Storage.Blob;
//using Newtonsoft.Json;

//namespace DemoAzureFunctionVisma.Functions
//{
//    public static class QueueFunction
//    {
//        [FunctionName("CreateProductWithQueue")]
//        public static async Task<IActionResult> CreateProductWithQueue(
//            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "product-queue")] HttpRequest httpRequest,
//            [Table("products", Connection = "AzureWebJobsStorage")] IAsyncCollector<ProductTableEntity> productTable,
//            [Queue("products", Connection = "AzureWebJobsStorage")] IAsyncCollector<ProductModel> productQueue,
//            ILogger logger)
//        {
//            logger.LogInformation("Creating new product and adding to queue");

//            var requestBody = await new StreamReader(httpRequest.Body).ReadToEndAsync();

//            var product = JsonConvert.DeserializeObject<ProductModel>(requestBody);

//            var productEntity = product.ToTableEntity();
//            product.Id = productEntity.RowKey;

//            await productTable.AddAsync(productEntity);
//            await productQueue.AddAsync(product);

//            return new OkResult();
//        }

//        [FunctionName("LogProduct")]
//        public static async Task LogProduct(
//            [QueueTrigger("products", Connection = "AzureWebJobsStorage")] ProductModel product,
//            [Blob("products", Connection = "AzureWebJobsStorage")] CloudBlobContainer cloudBlobContainer,
//            ILogger logger)
//        {
//            await cloudBlobContainer.CreateIfNotExistsAsync();

//            var blob = cloudBlobContainer.GetBlockBlobReference($"{product.Id}.txt");

//            var payload = JsonConvert.SerializeObject(product, Formatting.Indented);

//            await blob.UploadTextAsync($"New product created: \n \n {payload}");

//            logger.LogInformation($"Processed item: {product.Name}");
//        }
//    }
//}
