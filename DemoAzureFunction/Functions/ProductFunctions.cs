using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DemoAzureFunction.Application.Product;
using DemoAzureFunction.Entities;
using DemoAzureFunction.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;

namespace DemoAzureFunction.Functions
{
    public static class ProductFunctions
    {
        [FunctionName("HelloWorld")]
        public static async Task<string> HelloWorld(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "demo")] HttpRequest request,
            ILogger logger)
        {
            return await Task.FromResult("Tjena!");
        }

        [FunctionName("GetAllProducts")]
        public static async Task<List<ProductModel>> GetAllProducts(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "product")] HttpRequest request,
            [Table("products", Connection = "justkevserverlesstrgacc")] CloudTable productTable,
            ILogger logger)
        {
            logger.LogInformation("Getting all products");

            var query = new TableQuery<ProductTableEntity>();

            var segment = await productTable.ExecuteQuerySegmentedAsync(query, null);

            var products = segment.Select(ProductMapper.ToModel).ToList();

            return products;
        }

        [FunctionName("GetProduct")]
        public static IActionResult GetProduct(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "product/{id}")] HttpRequest request,
            [Table("products", "PRODUCT", "{id}", Connection = "justkevserverlesstrgacc")] ProductTableEntity productEntity,
            ILogger logger)
        {
            logger.LogInformation("Getting product by id");

            if (productEntity == null)
            {
                return new BadRequestResult();
            }

            var productModel = productEntity.ToModel();

            return new OkObjectResult(productModel);
        }

        [FunctionName("CreateProduct")]
        public static async Task<IActionResult> CreateProduct(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "product")] HttpRequest httpRequest,
            [Table("products", Connection = "justkevserverlesstrgacc")] IAsyncCollector<ProductTableEntity> productTable,
            ILogger logger)
        {
            logger.LogInformation("Creating new product");

            var requestBody = await new StreamReader(httpRequest.Body).ReadToEndAsync();

            var model = JsonConvert.DeserializeObject<ProductModel>(requestBody);

            var productEntity = model.ToTableEntity();

            await productTable.AddAsync(productEntity);

            return new OkResult();
        }

        [FunctionName("UpdateProduct")]
        public static async Task<IActionResult> UpdateProduct(
            [HttpTrigger(AuthorizationLevel.Anonymous, "put", Route = "product/{id}")] HttpRequest httpRequest,
            [Table("products", Connection = "justkevserverlesstrgacc")] CloudTable productTable,
            ILogger logger,
            string id)
        {
            logger.LogInformation("Updating product");

            var findOperation = TableOperation.Retrieve<ProductTableEntity>("PRODUCT", id);
            var findResult = await productTable.ExecuteAsync(findOperation);
            var currentProductEntity = (ProductTableEntity)findResult.Result;

            if (currentProductEntity == null)
            {
                return new BadRequestResult();
            }

            var requestBody = await new StreamReader(httpRequest.Body).ReadToEndAsync();

            var productModel = JsonConvert.DeserializeObject<ProductModel>(requestBody);

            ProductMapper.UpdateTableEntity(currentProductEntity, productModel);

            var updateOperation = TableOperation.Replace(currentProductEntity);

            await productTable.ExecuteAsync(updateOperation);

            return new OkResult();
        }

        [FunctionName("DeleteProduct")]
        public static async Task<IActionResult> DeleteProduct(
            [HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = "product/{id}")] HttpRequest httpRequest,
            [Table("products", Connection = "justkevserverlesstrgacc")] CloudTable productTable,
            ILogger logger,
            string id)
        {
            logger.LogInformation("Deleting product");

            var entityToDelete = new TableEntity
            {
                PartitionKey = "PRODUCT",
                RowKey = id,
                ETag = "*"
            };

            // ETag: Wildcard = delete whatever version. Concurrency check

            var deleteIoOperation = TableOperation.Delete(entityToDelete);

            try
            {
                await productTable.ExecuteAsync(deleteIoOperation);
            }

            catch (StorageException exception) when (exception.RequestInformation.HttpStatusCode == 404)
            {
                return new BadRequestResult();
            }

            return new OkResult();
        }

        //[FunctionName("GetAllProductsNoBinding")]
        //public static async Task<IActionResult> GetAllProductsNoBinding(
        //    [HttpTrigger(AuthorizationLevel.Anonymous, "get")] HttpRequest httpRequest,
        //    ILogger logger)
        //{
        //    var storageAccount = CloudStorageAccount.DevelopmentStorageAccount;
        //    //var storageAccount = CloudStorageAccount.Parse("CONNECTION_STRING");

        //    var tableClient = storageAccount.CreateCloudTableClient();

        //    var productTable = tableClient.GetTableReference("products");

        //    await productTable.CreateIfNotExistsAsync();

        //    var query = new TableQuery<ProductTableEntity>();

        //    var segment = await productTable.ExecuteQuerySegmentedAsync(query, null);

        //    var products = segment.Select(ProductMapper.ToModel);

        //    return new OkObjectResult(products);
        //}
    }
}

