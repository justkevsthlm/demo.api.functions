﻿//using System;
//using System.IO;
//using System.Threading.Tasks;
//using DemoAzureFunctionVisma.Entities;
//using DemoAzureFunctionVisma.Models;
//using Microsoft.AspNetCore.Http;
//using Microsoft.Azure.WebJobs;
//using Microsoft.Azure.WebJobs.Extensions.Http;
//using Microsoft.Extensions.Logging;
//using Newtonsoft.Json;

//namespace DemoAzureFunctionVisma.Functions
//{
//    public static class CustomerFunction
//    {
//        [FunctionName("AddCustomer")]
//        [return: Table("customers", Connection = "AzureWebJobsStorage")]
//        public static async Task<CustomersTableEntity> AddCustomer(
//            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "customer")] HttpRequest httpRequest,
//            ILogger logger)
//        {
//            logger.LogInformation("Adding new customer to database");

//            var requestBody = await new StreamReader(httpRequest.Body).ReadToEndAsync();
//            var model = JsonConvert.DeserializeObject<CustomerModel>(requestBody);

//            return new CustomersTableEntity
//            {
//                PartitionKey = "CUSTOMER",
//                RowKey = Guid.NewGuid().ToString(),
//                CreatedTime = DateTime.Now,
//                Name = $"{model.FirstName} {model.LastName}"
//            };
//        }
//    }
//}
