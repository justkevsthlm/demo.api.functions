//using System;
//using System.Collections.Generic;
//using System.Threading.Tasks;
//using DemoAzureFunctionVisma.Entities;
//using Microsoft.Azure.WebJobs;
//using Microsoft.Extensions.Logging;
//using Microsoft.WindowsAzure.Storage.Table;

//namespace DemoAzureFunctionVisma.Functions
//{
//    public static class ScheduleFunction
//    {
//        [FunctionName("DeleteOutDatedProductsJob")]
//        public static async Task DeleteOutDatedProductsJob(
//            [TimerTrigger("0 */30 * * * *")]TimerInfo timer,
//            [Table("products", Connection = "AzureWebJobsStorage" )] CloudTable productTable,
//            ILogger logger)
//        {
//            // Execute each 30 min according to CRON expression

//            var query = new TableQuery<ProductTableEntity>();

//            var segment = await productTable.ExecuteQuerySegmentedAsync(query, null);

//            var deleteTasks = new List<Task<TableResult>>();

//            var deleted = 0;

//            foreach (var productEntity in segment)
//            {
//                if (productEntity.OutDated)
//                {
//                    var deleteOperation = TableOperation.Delete(productEntity);

//                    var deleteTask = productTable.ExecuteAsync(deleteOperation);

//                    deleteTasks.Add(deleteTask);

//                    deleted++;
//                }
//            }

//            await Task.WhenAll(deleteTasks);

//            logger.LogInformation($"Deleted {deleted} products at: {DateTime.Now}");
//        }

//    }
//}
