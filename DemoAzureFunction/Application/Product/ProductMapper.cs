﻿using System;
using DemoAzureFunction.Entities;
using DemoAzureFunction.Models;

namespace DemoAzureFunction.Application.Product
{
    public static class ProductMapper
    {
        public static void UpdateTableEntity(ProductTableEntity periodEntity, ProductModel productModel)
        {
            periodEntity.Name = productModel.Name;
            periodEntity.Description = productModel.Description;
            periodEntity.Price = productModel.Price;
            periodEntity.OutDated = productModel.OutDated;
        }

        public static ProductTableEntity ToTableEntity(this ProductModel productModel)
        {
            return new ProductTableEntity
            {
                PartitionKey = "PRODUCT",
                RowKey =  Guid.NewGuid().ToString(),
                CreatedTime = DateTime.Now,
                Name = productModel.Name,
                Description = productModel.Description,
                Price = productModel.Price,
                OutDated = productModel.OutDated
            };
        }

        public static ProductModel ToModel(this ProductTableEntity productEntity)
        {
            return new ProductModel
            {
                Id = productEntity.RowKey,
                Name = productEntity.Name,
                Description = productEntity.Description,
                Price = productEntity.Price,
                OutDated = productEntity.OutDated
            };
        }
    }
}
